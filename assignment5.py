#!/usr/bin/env python3

# ------------------------------------------------------------------
# assignments/assignment5.py
# Fares Fraij
# ------------------------------------------------------------------

#-----------
# imports
#-----------

from flask import Flask, render_template, request, redirect, url_for, jsonify
import random
import json

app = Flask(__name__)

books = [{'title': 'Software Engineering', 'id': '1'}, \
		 {'title': 'Algorithm Design', 'id':'2'}, \
		 {'title': 'Python', 'id':'3'}]

@app.route('/book/JSON/')
def bookJSON():
	r = json.dumps(books)
	return jsonify(r)

@app.route('/')
@app.route('/book/')
def showBook():
	return render_template('showBook.html', books = books)

@app.route('/book/new/', methods=['GET', 'POST'])
def newBook():
	if request.method == "POST":
		id = 1
		for i in range(len(books)):
			if int(books[i].get("id")) != id:
				break
			id += 1
		new_title = request.form['name']
		newDict = {'title': new_title, 'id': id}
		index = id - 1
		books.insert(index, newDict)
		return render_template('showBook.html', books = books)
	else:
		return render_template('newBook.html', books = books)

@app.route('/book/<int:book_id>/edit/', methods=['GET','POST'])
def editBook(book_id):
	if request.method == 'GET':
		return render_template('editBook.html', book_id = book_id)
	elif request.method == 'POST':
		title = request.form['name']
		for i in books:
			if int(i.get("id")) == int(book_id):
				i["title"] = title
		return render_template('showBook.html', books = books)
	else:
		return render_template('showBook.html', books = books)

@app.route('/book/<int:book_id>/delete/', methods = ['GET', 'POST'])
def deleteBook(book_id):
	if request.method == 'GET':
		for i in books:
			if int(i.get("id")) == int(book_id):
				name = i.get("title")
		return render_template('deleteBook.html', book_id = book_id, name = name)
	elif request.method == 'POST':
		for i in range(len(books)):
			if int(books[i].get("id")) == int(book_id):
				del books[i]
				break
		return render_template('showBook.html', books = books)
	else:
		return render_template('showBook.html', books = books)

if __name__ == '__main__':
	app.debug = True
	app.run(host = '0.0.0.0', port = 5000)
